import {Issue} from './issue.model';

export interface ApiResponse {
  total_count: number;
  incomplete_results: boolean;
  current_page: number;
  pages_num: number;
  items: Issue[];

}
