export interface FavouriteIssue {
  id: number;
  title: string;
}
