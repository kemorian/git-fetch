import {Injectable} from '@angular/core';
import {FavouriteIssue} from "../models/favourite-issue.model";

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() {
  }

  saveFavorites(favorites: FavouriteIssue[]) {
    localStorage.setItem('favorites', JSON.stringify(favorites));
  }

  fetchFavorites(): FavouriteIssue[] {
    const favorites: FavouriteIssue[] = JSON.parse(localStorage.getItem('favorites'));

    return favorites ? favorites : [];
  }

}
