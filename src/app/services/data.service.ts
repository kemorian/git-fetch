import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ApiResponse} from '../models/response.model';
import {IssuesService} from '../issues/issues.service';
import {Subject} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  keywordSearching = new Subject<{ searching: boolean, keyword: string, error: boolean }>();
  searchKeyword = '';
  fetchingError = false;

  constructor(private http: HttpClient, private issuesService: IssuesService) {
  }

  /**
   * Find issues by given keyword
   * @param keyword - keyword to search by
   * @param page - requested page of results
   */
  fetchIssues(keyword: string, page: number = 1) {
    this.keywordSearching.next({searching: true, keyword: this.searchKeyword, error: false});
    this.searchKeyword = keyword;
    this.fetchingError = false;
    return this.http
      .get<ApiResponse>(
        `https://api.github.com/search/issues?q=${keyword}+state:open&page=${page}`
      ).pipe(
        map(res => {
          return {
            ...res,
            current_page: page,
            pages_num: Math.ceil(res.total_count / res.items.length)
          };
        })
      )
      .subscribe(res => {
        this.issuesService.setData(res.items, res.total_count, res.current_page, res.pages_num);
        this.keywordSearching.next({searching: false, keyword: this.searchKeyword, error: false});
      }, error => {
        this.fetchingError = true;
        this.keywordSearching.next({searching: false, keyword: this.searchKeyword, error: true});
      });
  }

  /**
   * Fetch another page of currently searched keyword
   * @param page - requested page number
   */
  fetchPage(page: number) {
    this.fetchIssues(this.searchKeyword, page);
  }
}
