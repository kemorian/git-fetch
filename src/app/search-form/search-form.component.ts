import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from './CustomValidators';
import {DataService} from '../services/data.service';
import {Subject, Subscription} from 'rxjs';
import {auditTime, debounceTime} from "rxjs/operators";

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit, OnDestroy {
  keywordForm: FormGroup;
  inputSubject: Subject<string> = new Subject<string>();
  subscription: Subscription;
  initialKeyword: string;

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.initialKeyword = this.dataService.searchKeyword;
    this.initForm();
    this.subscription = this.inputSubject.asObservable().pipe(
      // limit requests to max 1 per second
      auditTime(1000)
    )
      .subscribe(keyword => this.dataService.fetchIssues(keyword));
  }

  onSubmit() {
    if (this.keywordForm.valid) {
      this.inputSubject.next(this.keywordForm.value.keyword.trim());
    }

  }

  private initForm() {
    const keyword = '';
    this.keywordForm = new FormGroup({
      keyword: new FormControl(keyword, [Validators.required, CustomValidators.validateTrimmedLength])
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
