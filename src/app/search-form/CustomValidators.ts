import {FormControl} from '@angular/forms';

export class CustomValidators {

  /**
   * Validates if form control trimmed value length is bigger or equal to 2
   * @param c - passed form control
   */
  static validateTrimmedLength(c: FormControl) {
    return c.value.trim().length >= 2 ? null : {
      validateLength: {
        valid: false
      }
    };
  }
}
