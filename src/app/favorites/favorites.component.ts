import {Component, OnInit} from '@angular/core';
import {IssuesService} from '../issues/issues.service';
import {FavouriteIssue} from '../models/favourite-issue.model';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent implements OnInit {

  favorites: FavouriteIssue[] = [];

  constructor(private issuesService: IssuesService) {
  }

  ngOnInit() {
    this.favorites = this.issuesService.getFavorites();
  }

  favouriteClickHandler(issue) {
    this.issuesService.removeFromFavorites(issue.id);
    this.favorites = this.issuesService.getFavorites();
  }
}
