import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Issue} from '../../models/issue.model';

@Component({
  selector: 'app-issue',
  templateUrl: './issue.component.html',
  styleUrls: ['./issue.component.scss']
})
export class IssueComponent implements OnInit {
  @Input() issue: Issue;
  @Output() favouriteToggled: EventEmitter<null> = new EventEmitter();


  constructor() {
  }

  ngOnInit() {
  }

  favouriteClickHandler() {
    this.favouriteToggled.emit();
  }


}
