import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  @Input() currentPage: number;
  @Input() listLength: number;
  @Input() totalCount: number;
  @Input() numPages: number;
  @Output() pageChanged: EventEmitter<number> = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  nextPageClickHandler() {
    if (this.currentPage < this.numPages) {
      this.pageChanged.emit(this.currentPage + 1);
    }

  }

  prevPageClickHandler() {
    if (this.currentPage > 1) {
      this.pageChanged.emit(this.currentPage - 1);
    }
  }
}
