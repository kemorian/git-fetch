import {Injectable} from '@angular/core';
import {Issue} from '../models/issue.model';
import {Subject} from 'rxjs';
import {FavouriteIssue} from '../models/favourite-issue.model';
import {LocalStorageService} from '../services/local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class IssuesService {

  issuesChanged = new Subject<Issue[]>();

  private issues: Issue[] = [];
  private _totalCount = 0;
  private _currentPage;
  private _numPages;

  private favoriteIssues: FavouriteIssue[] = [];

  constructor(private localStorageService: LocalStorageService) {
    this.favoriteIssues = localStorageService.fetchFavorites();
  }

  /**
   * Set issue data, emit next od issues subject
   * @param issues - issues list
   * @param totalCount - total issues count
   * @param page - current results page
   * @param numPages - nupber of results pages
   */
  setData(issues: Issue[], totalCount: number, page: number, numPages: number) {
    this.issues = issues.map(issue => {
      return {
        ...issue,
        favorite: this.favoriteIssues.find(iss => iss.id === issue.id) !== undefined
      };
    });
    this._totalCount = totalCount;
    this._currentPage = page;
    this._numPages = numPages;
    this.issuesChanged.next(this.issues.slice());
  }

  get totalCount(): number {
    return this._totalCount;
  }
  get currentPage(): number {
    return this._currentPage;
  }
  get numPages(): number {
    return this._numPages;
  }

  getIssues(): Issue[] {
    return this.issues.slice();
  }

  toggleFavorite(issue: Issue) {
    issue.favorite = !issue.favorite;
    if (issue.favorite) {
      this.favoriteIssues.push({id: issue.id, title: issue.title});
    } else {
      this.favoriteIssues = this.favoriteIssues.filter(fIssue => fIssue.id !== issue.id);
    }
    this.localStorageService.saveFavorites(this.favoriteIssues);
  }

  removeFromFavorites(issueId: number) {
    this.favoriteIssues = this.favoriteIssues.filter(fIssue => fIssue.id !== issueId);
    this.issues = this.issues.map(issue => {
      if (issue.id === issueId) {
        issue.favorite = false;
      }
      return issue;
    });
    this.localStorageService.saveFavorites(this.favoriteIssues);
  }

  getFavorites(): FavouriteIssue[] {
    return this.favoriteIssues.slice();
  }
}
