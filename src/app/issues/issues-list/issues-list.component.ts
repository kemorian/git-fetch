import {Component, OnDestroy, OnInit} from '@angular/core';
import {IssuesService} from '../issues.service';
import {Issue} from '../../models/issue.model';
import {Subscription} from 'rxjs';
import {DataService} from '../../services/data.service';

@Component({
  selector: 'app-issues-list',
  templateUrl: './issues-list.component.html',
  styleUrls: ['./issues-list.component.scss']
})
export class IssuesListComponent implements OnInit, OnDestroy {

  issues: Issue[];
  totalCount = 0;
  currentPage = 0;
  numPages = 0;
  issuesSubscription: Subscription;
  searchSubscription: Subscription;
  searching: boolean;


  constructor(private issuesService: IssuesService, private dataService: DataService) {
  }

  ngOnInit() {
    this.issuesSubscription = this.issuesService.issuesChanged
      .subscribe(
        (issues: Issue[]) => {
          this.issues = issues;
          this.totalCount = this.issuesService.totalCount;
          this.currentPage = this.issuesService.currentPage;
          this.numPages = this.issuesService.numPages;
        }
      );
    this.searchSubscription = this.dataService.keywordSearching
      .subscribe(
        (data: any) => {
          this.searching = data.searching;
        }
      );
    this.issues = this.issuesService.getIssues();
    this.totalCount = this.issuesService.totalCount;
    this.currentPage = this.issuesService.currentPage;
    this.numPages = this.issuesService.numPages;

  }

  toggleFavorite(issue: Issue) {
    this.issuesService.toggleFavorite(issue);
  }

  ngOnDestroy(): void {
    if (this.issuesSubscription) {
      this.issuesSubscription.unsubscribe();
    }
    if (this.searchSubscription) {
      this.searchSubscription.unsubscribe();
    }
  }

  pageChanged(requestedPage: number) {
    console.log(requestedPage);
    this.dataService.fetchPage(requestedPage);
  }
}
