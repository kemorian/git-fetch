import {Component, OnDestroy, OnInit} from '@angular/core';
import {DataService} from '../services/data.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  subscription: Subscription;
  keyword: string;
  showSpinner: boolean;
  error = false;

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.subscription = this.dataService.keywordSearching.subscribe(
      (data: { searching: boolean, keyword: string, error: boolean }) => {
        this.keyword = data.keyword;
        this.showSpinner = data.searching;
        this.error = data.error;
      }
    );
    this.keyword = this.dataService.searchKeyword;
    this.error = this.dataService.fetchingError;
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
